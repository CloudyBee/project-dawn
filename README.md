# Project Dawn

An implementation of the vaguely defined 'ECS', or 'Entity Component System';. A concept of game objects being created out of many components which define behaviour, as opposed to inheriting behaviour from parent objects.